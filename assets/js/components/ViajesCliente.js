import React, {Component} from 'react';
import {Route, Switch, Link} from 'react-router-dom';
import axios from 'axios';
    
class ViajesCliente extends Component {
    constructor() {
        super();
        this.state = { viajes: [], loading: true};
    }
    
    componentDidMount() {
        this.getViajes();
    }
    
    getViajes() {
       axios.get(`http://localhost:8000/GetViajesClientes?cedula=`+this.props.match.params.cedula).then(viajes => {
            this.setState({ viajes: viajes.data.data, loading: false})               
       })
    }
    
    render() {
        const loading = this.state.loading;
        return(
            <div>
                <section className="row-section">
                    <div className="container">
                    <div className="row">
                            <h2 className="text-center"><span>Lista de Viajes del Cliente </span></h2>                           
                    </div>
                    <div className="media-right align-self-center">                                
                                <li className="nav-item">
                                 <Link className={"nav-link"} to={"/ViajeClienteCreate"}>Nuevo</Link>
                           </li>
                     </div>
                            {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>                                
                            </div>
                        ) : (
                            <div className={'row'}>
                                { this.state.viajes.map(viaje=>
                                    <div className="col-md-10 offset-md-1 row-block" key={viaje.id}>
                                        <ul id="sortable">
                                            <li>
                                                <div className="media">                                                   
                                                    <div className="media-body">
                                                        <h4>{viaje.codigo}</h4>                                                        
                                                    </div>                                                                      
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </section>

            </div>
     
        )
    }
}    
   
export default ViajesCliente;


