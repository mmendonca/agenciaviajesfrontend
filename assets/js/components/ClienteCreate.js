import React, {Component} from 'react';
import axios from 'axios';

class ClienteCreate extends React.Component {
    constructor(props) {
        super(props);   
         
        this.state = {
            cedula: '',
            nombre: '',
            telefono: '',
            fecnac: ''
          };
      
          this.handleChange = this.handleChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
        }
        handleChange(event)  {
            const { name, value } = event.target
            this.setState({ [name]: value })
          }
          
          handleSubmit (event)  {
            event.preventDefault()
            const values = JSON.stringify(this.state)
            var obj= JSON.parse(values);
            var cedula = obj['cedula']
            var nombre = obj['nombre']
            var telefono = obj['telefono']
            var fecnac = obj['fecnac']
            
            axios.get(`http://localhost:8000/CreateCliente?cedula=`+cedula+`&nombre=`+nombre+`&telefono=`+telefono+`&fecnac=`+fecnac)
            return this.props.history.push('/Clientes');
            
          }



  
    render() {
        const { nombre, telefono } = this.state
      return (
        <div>
        <section className="row-section">
            <div className="container">
                <div className="row">
                    <h2 className="text-center"><span>Cliente Nuevo</span> </h2>                                                    
                </div>
                </div>     
         </section>
                <div className="media-right align-self-center">                                        
                   <form onSubmit={this.handleSubmit}>
                        <div>
                            <label htmlFor="name">Cedula:</label>
                            <input  name="cedula" type="text" onChange={this.handleChange} />
                    </div>
                        <div>
                        <label htmlFor="name">Nombre:</label>
                            <input  name="nombre"  type="text" onChange={this.handleChange} />
                    </div>
                    <div>
                        <label htmlFor="name">Telefono:</label>   
                        <input  name="telefono" type="text"  onChange={this.handleChange} />        
                        </div>
                        <div>
                            <label htmlFor="name">Fecha Nacimiento:</label>     
                            <input  name="fecnac" type="text" onChange={this.handleChange} />
                        </div>
                        <div>
                            <input type="submit" value="Agregar" />
                        </div>
                    </form>
                    </div>                    

      </div>
        );
        
     

      

    
    
    }

}

export default ClienteCreate;