import React, {Component} from 'react';
import {Route, Switch,Redirect, Link } from 'react-router-dom';
import Clientes from './Clientes';
import Viajes from './Viajes';
import ClienteView from './ClienteView';
import ClienteDelete from './ClienteDelete';
import ClienteCreate from './ClienteCreate';
import ViajesCliente from './ViajesCliente';
import ViajeClienteCreate from './ViajeClienteCreate';



    
class Home extends Component {
    
    render() {
        return (                        
            <div>
               <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                   <Link className={"navbar-brand"} to={"/"}> Agencia de Viajes </Link>
                   <div className="collapse navbar-collapse" id="navbarText">
                       <ul className="navbar-nav mr-auto">                           
                           <li className="nav-item">
                               <Link className={"nav-link"} to={"/Clientes"}> Clientes </Link>
                           </li>
                           <li className="nav-item">
                               <Link className={"nav-link"} to={"/Viajes"}> Viajes </Link>
                           </li>                                  
                       </ul>
                   </div>
               </nav>
               <Switch>                   
                   <Redirect exact from="/" to="/viajes" />                           
                   <Route path="/clientes" component={Clientes} />  
                   <Route path="/clienteView/:cedula" component={ClienteView} />  
                   <Route path="/clienteDelete/:cedula" component={ClienteDelete}/>
                   <Route path="/clienteCreate" component={ClienteCreate}/>
                   <Route path="/Viajes" component={Viajes}/>
                   <Route path="/ViajesCliente/:cedula" component={ViajesCliente}/>
                   <Route path="/ViajeClienteCreate" component={ViajeClienteCreate}/>
               </Switch>
           </div>
        )
    }
}
    
export default Home;