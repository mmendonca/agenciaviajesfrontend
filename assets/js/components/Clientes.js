import React, {Component} from 'react';
import {Route, Switch, Link} from 'react-router-dom';
import axios from 'axios';

    
class Clientes extends Component {
    constructor() {
        super();
        this.state = { clientes: [], loading: true};
    }
    
    componentDidMount() {
        this.getClientes();
    }
    
    getClientes() {
       axios.get(`http://localhost:8000/GetClientes`).then(clientes => {
           this.setState({ clientes: clientes.data.data, loading: false})
       })
    }
    
    render() {
        const loading = this.state.loading;
        return(
            <div>
                <section className="row-section">
                    <div className="container">
                        <div className="row">
                            <h2 className="text-center"><span>Lista de Clientes</span> </h2>                                                    
                        </div>
                        <div className="media-right align-self-center">                                
                                <li className="nav-item">
                                 <Link className={"nav-link"} to={"/ClienteCreate"}>Nuevo</Link>
                                </li>
                             </div>
                        {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>
                            </div>
                        ) : (
                            <div className={'row'}>
                                { this.state.clientes.map(cliente=>
                                    <div className="col-md-10 offset-md-1 row-block" key={cliente.id}>
                                        <ul id="sortable">
                                            <li>
                                                <div className="media">                                                   
                                                    <div className="media-body">
                                                        <h4>{cliente.cedula} - {cliente.nombre}</h4>                                                        
                                                    </div>                                                                                                                                                        
                                                    <li className="nav-item">
                                                     <Link className={"nav-link"} to={"/ClienteView/"+cliente.cedula}>Ver</Link>
                                                     </li>
                                                     <li className="nav-item">
                                                     <Link className={"nav-link"} to={"/ClienteDelete/"+cliente.cedula}>Eliminar</Link>
                                                     </li>
                                                     <li className="nav-item">
                                                     <Link className={"nav-link"} to={"/ViajesCliente/"+cliente.cedula}>*Viajes*</Link>
                                                     </li>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </section>

            </div>
     
        )
    }
}
export default Clientes;