import React, {Component} from 'react';
import axios from 'axios';

class ViajeClienteCreate extends React.Component {
    constructor(props) {
        super(props);   
         
        this.state = {
            cedula: '',
            codigo: '',            
          };
      
          this.handleChange = this.handleChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
        }
        handleChange(event)  {
            const { name, value } = event.target
            this.setState({ [name]: value })
          }
          
          handleSubmit (event)  {
            event.preventDefault()
            const values = JSON.stringify(this.state)
            var obj= JSON.parse(values);
            var cedula = obj['cedula']
            var codigo = obj['codigo']
            
            axios.get(`http://localhost:8000/AsignaViaje?cedula=`+cedula+`&codigo=`+codigo)
            return this.props.history.push('/Clientes');
            
          }



  
    render() {
        const { nombre, telefono } = this.state
      return (
        <div>
        <section className="row-section">
            <div className="container">
                <div className="row">
                    <h2 className="text-center"><span>Asignar Viaje a Cliente</span> </h2>                                                    
                </div>
                </div>     
         </section>
                <div className="media-right align-self-center">                                        
                   <form onSubmit={this.handleSubmit}>
                        <div>
                            <label htmlFor="name">Cedula del Cliente:</label>
                            <input  name="cedula" type="text" onChange={this.handleChange} />
                    </div>
                        <div>
                        <label htmlFor="name">Codigo Viaje:</label>
                            <input  name="codigo"  type="text" onChange={this.handleChange} />
                    </div>                   
                        <div>
                            <input type="submit" value="Asignar" />
                        </div>
                    </form>
                    </div>                    

      </div>
        );
        
     

      

    
    
    }

}

export default ViajeClienteCreate;