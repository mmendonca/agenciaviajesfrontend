import React, {Component} from 'react';
import axios from 'axios';
    
class ClienteView extends Component {
    constructor(props) {
        super();   
         
        this.state = { cliente:[], loading: true};
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleSubmit(event) {
        event.preventDefault();         
      

      }
      

    componentDidMount() {
        this.getCliente();
    }
    
    getCliente() {
       axios.get(`http://localhost:8000/GetCliente?cedula=`+this.props.match.params.cedula).then(cliente => {
           this.setState({ cliente: cliente.data.data, loading: false})
       })
    }
    
    render() {
        const loading = this.state.loading;
        return(          
        <div> 
           <section className="row-section">
            <div className="container">
                <div className="row">
                    <h2 className="text-center"><span>Datos del Cliente</span> </h2>                                                    
                </div>
              </div>     
            </section>
                 <div>                                      
                        { this.state.cliente.map(cliente=>
                        <form onSubmit={this.handleSubmit}>   
                                <div>
                                    <label className="media-body"> Cedula:  {cliente.cedula} </label>
                                </div>
                                <div>                            
                                        <label className="media-body"> Nombre : <input name="nombre" type="text" value={cliente.nombre} /> </label>
                                </div>   
                                <div>                                                                             
                                        <label className="media-body"> Telefono: <input name="telefono" type="text" value={cliente.telefono} /> </label>
                                </div>
                                <div>                                                                             
                                        <label className="media-body"> Fecha Nacimiento: <input name="fecnac" type="text" value={cliente.fecnac} /> </label>
                                </div>
                                <input type="submit" value="Actualizar" /> 
                         </form>
                            )}                        
                  </div>
        </div> 
        )
    }
  
 
}

export default ClienteView;