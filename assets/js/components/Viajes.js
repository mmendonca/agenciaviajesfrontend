import React, {Component} from 'react';
import {Route, Switch, Link} from 'react-router-dom';
import axios from 'axios';

    
class Viajes extends Component {
    constructor() {
        super();
        this.state = { viajes: [], loading: true};
    }
    
    componentDidMount() {
        this.getViajes();
    }
    
    getViajes() {
       axios.get(`http://localhost:8000/GetViajes`).then(viajes => {
           this.setState({ viajes: viajes.data.data, loading: false})
       })
    }
    
    render() {
        const loading = this.state.loading;
        return(
            <div>
                <section className="row-section">
                    <div className="container">
                        <div className="row">
                            <h2 className="text-center"><span>Lista de Viajes</span></h2>
                        </div>
                        {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>                                
                            </div>
                        ) : (
                            <div className={'row'}>
                                { this.state.viajes.map(viaje=>
                                    <div className="col-md-10 offset-md-1 row-block" key={viaje.id}>
                                        <ul id="sortable">
                                            <li>
                                                <div className="media">                                                   
                                                    <div className="media-body">
                                                        <h4>{viaje.codigo} : {viaje.origen}- {viaje.destino} Plazas: {viaje.plazas} Precio: {viaje.precio}</h4>                                                        
                                                    </div>                                                   
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </section>

            </div>
     
        )
    }
}
export default Viajes;